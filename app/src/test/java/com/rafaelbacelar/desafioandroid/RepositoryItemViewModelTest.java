package com.rafaelbacelar.desafioandroid;

import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.view.View;

import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.model.User;
import com.rafaelbacelar.desafioandroid.viewmodel.RepositoryItemViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by rafaelbacelar on 13/10/16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class RepositoryItemViewModelTest {

    DesafioApplication application;

    @Before
    public void setUp() {
        application = (DesafioApplication) RuntimeEnvironment.application;
    }

    @Test
    public void shouldGetName() {
        Repository repository = new Repository();
        repository.setName("react-native");
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(repository.getName(), repositoryItemViewModel.getName());
    }

    @Test
    public void shouldGetDescription() {
        Repository repository = new Repository();
        repository.setDescription("A framework for building native apps with React.");
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(repository.getDescription(), repositoryItemViewModel.getDescription());
    }

    @Test
    public void shouldGetStars() {
        Repository repository = new Repository();
        repository.setStars(38851);
        String expectedString = String.valueOf(repository.getStars());
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(expectedString, repositoryItemViewModel.getStars());
    }

    @Test
    public void shouldGetForks() {
        Repository repository = new Repository();
        repository.setForks(8681);
        String expectedString = String.valueOf(repository.getForks());
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(expectedString, repositoryItemViewModel.getForks());
    }

    @Test
    public void shouldGetUsername() {
        Repository repository = new Repository();
        User user = new User(){{ setLogin("facebook");}};
        repository.setOwner(user);
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(user.getLogin(), repositoryItemViewModel.getUsername());
    }

    @Test
    public void shouldGetUserAvatar() {
        Repository repository = new Repository();
        User user = new User(){{ setAvatarUrl("https://avatars.githubusercontent.com/u/69631?v=3");}};
        repository.setOwner(user);
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        assertEquals(user.getAvatarUrl(), repositoryItemViewModel.getUserAvatarUrl());
    }

    @Test
    public void shouldStartActivityOnItemClick() {
        Repository repository = new Repository();
        Context mockContext = mock(Context.class);
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(mockContext, repository);
        repositoryItemViewModel.onItemClick(new View(application));
        verify(mockContext).startActivity(any(Intent.class));
    }

    @Test
    public void shouldNotifyPropertyChangeWhenSetRepository() {
        Repository repository = new Repository();
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        Observable.OnPropertyChangedCallback mockCallback =
                mock(Observable.OnPropertyChangedCallback.class);
        repositoryItemViewModel.addOnPropertyChangedCallback(mockCallback);

        repositoryItemViewModel.setRepository(repository);
        verify(mockCallback).onPropertyChanged(any(Observable.class), anyInt());
    }
}
