package com.rafaelbacelar.desafioandroid;

import android.content.Context;
import android.content.Intent;
import android.databinding.Observable;
import android.view.View;

import com.rafaelbacelar.desafioandroid.model.Pull;
import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.model.User;
import com.rafaelbacelar.desafioandroid.viewmodel.PullItemViewModel;
import com.rafaelbacelar.desafioandroid.viewmodel.RepositoryItemViewModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by rafaelbacelar on 13/10/16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class PullItemViewModelTest {

    SimpleDateFormat dateFormat;

    DesafioApplication application;

    @Before
    public void setUp() {
        application = (DesafioApplication) RuntimeEnvironment.application;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    }

    @Test
    public void shouldGetTitle() {
        Pull pull = new Pull();
        pull.setTitle("Added support for textColor in DatePickerIOS");
        PullItemViewModel pullItemViewModel = new PullItemViewModel(application, pull, dateFormat);
        assertEquals(pull.getTitle(), pullItemViewModel.getTitle());

    }

    @Test
    public void shouldGetCreationDate() {
        Pull pull = new Pull();

        Calendar calendar = Calendar.getInstance();
        calendar.set(2016,9,12,11,15);
        pull.setCreatedAt(new Date(calendar.getTimeInMillis()));
        String expectedString = "2016-10-12 11:15";

        PullItemViewModel pullItemViewModel = new PullItemViewModel(application, pull, dateFormat);
        assertEquals(expectedString, pullItemViewModel.getCreatedAt());

    }

    @Test
    public void shouldGetBody() {
        Pull pull = new Pull();
        pull.setBody("This update allows the text/label color of the DatePickerIOS to be updated via an attribute called");
        PullItemViewModel pullItemViewModel = new PullItemViewModel(application, pull, dateFormat);
        assertEquals(pull.getBody(), pullItemViewModel.getBody());

    }

    @Test
    public void shouldGetUsername() {
        Pull pull = new Pull();
        User user = new User(){{ setLogin("facebook");}};
        pull.setUser(user);
        PullItemViewModel pullItemViewModel = new PullItemViewModel(application, pull, dateFormat);
        assertEquals(user.getLogin(), pullItemViewModel.getUsername());
    }

    @Test
    public void shouldGetUserAvatar() {
        Pull pull = new Pull();
        User user = new User(){{ setAvatarUrl("https://avatars.githubusercontent.com/u/69631?v=3");}};
        pull.setUser(user);
        PullItemViewModel pullItemViewModel = new PullItemViewModel(application, pull, dateFormat);
        assertEquals(user.getAvatarUrl(), pullItemViewModel.getUserAvatarUrl());
    }

    @Test
    public void shouldStartBrowserOnItemClick() {
        Pull pull = new Pull();
        pull.setUrl("https://github.com/facebook/react-native/pull/10356");
        Context mockContext = mock(Context.class);
        PullItemViewModel pullItemViewModel = new PullItemViewModel(mockContext, pull, dateFormat);
        pullItemViewModel.onItemClick(new View(application));
        verify(mockContext).startActivity(any(Intent.class));
    }

    @Test
    public void shouldNotifyPropertyChangeWhenSetRepository() {
        Repository repository = new Repository();
        RepositoryItemViewModel repositoryItemViewModel = new RepositoryItemViewModel(application, repository);
        Observable.OnPropertyChangedCallback mockCallback =
                mock(Observable.OnPropertyChangedCallback.class);
        repositoryItemViewModel.addOnPropertyChangedCallback(mockCallback);

        repositoryItemViewModel.setRepository(repository);
        verify(mockCallback).onPropertyChanged(any(Observable.class), anyInt());
    }
}
