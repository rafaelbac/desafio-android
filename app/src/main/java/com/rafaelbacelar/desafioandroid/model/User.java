package com.rafaelbacelar.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class User {
    private int id;
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
