package com.rafaelbacelar.desafioandroid.model;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public interface GithubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<RepositoryList> getJavaRepositoriesByPage(@Query("page") int page);

    @GET("repos/{user}/{repository}/pulls")
    Observable<List<Pull>> getPullsByRepository(@Path("user") String user, @Path("repository") String repository);
}
