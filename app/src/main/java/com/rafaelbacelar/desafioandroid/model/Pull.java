package com.rafaelbacelar.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class Pull {
    private int id;
    private String title;
    private String body;
    private String state;
    @SerializedName("created_at")
    private Date createdAt;
    @SerializedName("html_url")
    private String url;
    private User user;

    public Pull() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
