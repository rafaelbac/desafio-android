package com.rafaelbacelar.desafioandroid.model;


import com.google.gson.annotations.SerializedName;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class Repository {
    private int id;
    private String name;
    private String description;
    @SerializedName("stargazers_count")
    private int stars;
    private int forks;
    private User owner;

    public Repository() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }


}
