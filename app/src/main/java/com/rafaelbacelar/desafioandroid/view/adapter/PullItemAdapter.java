package com.rafaelbacelar.desafioandroid.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.databinding.PullItemBinding;
import com.rafaelbacelar.desafioandroid.model.Pull;
import com.rafaelbacelar.desafioandroid.viewmodel.PullItemViewModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class PullItemAdapter extends RecyclerView.Adapter<PullItemAdapter.PullViewHolder> {

    private List<Pull> pulls;

    public PullItemAdapter() {
        this.pulls = new ArrayList<>();
    }

    public PullItemAdapter(List<Pull> pulls) {
        this.pulls = pulls;
    }

    public void setPulls(List<Pull> pulls) {
        this.pulls = pulls;
    }

    @Override
    public PullViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PullItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.pull_item,
                parent,
                false);
        return new PullViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PullViewHolder holder, int position) {
        holder.bindPull(pulls.get(position));
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public static class PullViewHolder extends RecyclerView.ViewHolder {
        private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

        final PullItemBinding binding;

        public PullViewHolder(PullItemBinding binding) {
            super(binding.layoutPull);
            this.binding = binding;
        }

        void bindPull(Pull pull) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new PullItemViewModel(itemView.getContext(), pull, dateFormat));
            } else {
                binding.getViewModel().setPull(pull);
            }
        }
    }
}
