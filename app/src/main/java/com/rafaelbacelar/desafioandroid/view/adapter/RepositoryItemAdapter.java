package com.rafaelbacelar.desafioandroid.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.databinding.RepositoryItemBinding;
import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.viewmodel.RepositoryItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class RepositoryItemAdapter extends RecyclerView.Adapter<RepositoryItemAdapter.RepositoryViewHolder> {

    private List<Repository> repositories;

    public RepositoryItemAdapter() {
        this.repositories = new ArrayList<>();
    }

    public RepositoryItemAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RepositoryItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.repository_item,
                parent,
                false);
        return new RepositoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        holder.bindRepository(repositories.get(position));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public static class RepositoryViewHolder extends RecyclerView.ViewHolder {
        final RepositoryItemBinding binding;

        public RepositoryViewHolder(RepositoryItemBinding binding) {
            super(binding.layoutRepository);
            this.binding = binding;
        }

        void bindRepository(Repository repository) {
            if (binding.getViewModel() == null) {
                binding.setViewModel(new RepositoryItemViewModel(itemView.getContext(), repository));
            } else {
                binding.getViewModel().setRepository(repository);
            }
        }
    }
}
