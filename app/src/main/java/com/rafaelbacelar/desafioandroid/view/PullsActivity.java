package com.rafaelbacelar.desafioandroid.view;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.databinding.ActivityPullsBinding;
import com.rafaelbacelar.desafioandroid.model.Pull;
import com.rafaelbacelar.desafioandroid.view.adapter.PullItemAdapter;
import com.rafaelbacelar.desafioandroid.viewmodel.PullsViewModel;

import java.util.List;

public class PullsActivity extends AppCompatActivity implements PullsViewModel.DataListener {

    private static final String EXTRA_USERNAME = "EXTRA_USERNAME";
    private static final String EXTRA_REPOSITORY_NAME = "EXTRA_REPOSITORY_NAME";

    private ActivityPullsBinding binding;
    private PullsViewModel pullsViewModel;

    public static Intent newIntent(Context context, String username, String repositoryName) {
        Intent intent = new Intent(context, PullsActivity.class);
        intent.putExtra(EXTRA_USERNAME, username);
        intent.putExtra(EXTRA_REPOSITORY_NAME, repositoryName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pulls);
        pullsViewModel = new PullsViewModel(this, this);
        binding.setViewModel(pullsViewModel);
        setSupportActionBar(binding.toolbar);
        setupRecyclerView(binding.recyclerView);

        String username = getIntent().getStringExtra(EXTRA_USERNAME);
        String repositoryName = getIntent().getStringExtra(EXTRA_REPOSITORY_NAME);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        setTitle(repositoryName);

        pullsViewModel.loadData(username, repositoryName);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pullsViewModel.destroy();
    }

    @Override
    public void onPullsChanged(List<Pull> pulls) {
        PullItemAdapter adapter = (PullItemAdapter) binding.recyclerView.getAdapter();
        adapter.setPulls(pulls);
        adapter.notifyDataSetChanged();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        PullItemAdapter adapter = new PullItemAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
