package com.rafaelbacelar.desafioandroid.view;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.databinding.ActivityMainBinding;
import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.view.adapter.RepositoryItemAdapter;
import com.rafaelbacelar.desafioandroid.view.listener.EndlessRecyclerViewScrollListener;
import com.rafaelbacelar.desafioandroid.viewmodel.MainViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainViewModel.DataListener {

    private ActivityMainBinding binding;
    private MainViewModel mainViewModel;
    private RecyclerView.OnScrollListener onScrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mainViewModel = new MainViewModel(this, this);
        binding.setViewModel(mainViewModel);
        setSupportActionBar(binding.toolbar);
        setupRecyclerView(binding.recyclerView);
        mainViewModel.loadData(1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainViewModel.destroy();
    }

    @Override
    public void onRepositoriesChanged(List<Repository> repositories) {
        RepositoryItemAdapter adapter =
                (RepositoryItemAdapter) binding.recyclerView.getAdapter();
        adapter.setRepositories(repositories);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onLoadingError(Throwable error) {
        ((EndlessRecyclerViewScrollListener)onScrollListener).stopLoading();
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        RepositoryItemAdapter adapter = new RepositoryItemAdapter();
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        onScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                mainViewModel.loadData(page);
            }
        };

        recyclerView.addOnScrollListener(onScrollListener);
    }
}
