package com.rafaelbacelar.desafioandroid;

import android.app.Application;
import android.content.Context;

import com.rafaelbacelar.desafioandroid.model.GithubService;
import com.rafaelbacelar.desafioandroid.model.GithubServiceFactory;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class DesafioApplication extends Application {
    private GithubService githubService;
    private Scheduler scheduler;

    public static DesafioApplication get(Context context) {
        return (DesafioApplication) context.getApplicationContext();
    }

    public GithubService getGithubService() {
        if (githubService == null) {
            githubService = GithubServiceFactory.create(this);
        }
        return githubService;
    }

    public void setGithubService(GithubService githubService) {
        this.githubService = githubService;
    }

    public Scheduler getScheduler() {
        if (scheduler == null) {
            scheduler = Schedulers.io();
        }
        return scheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
