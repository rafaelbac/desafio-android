package com.rafaelbacelar.desafioandroid.viewmodel;


/**
 * Created by rafaelbacelar on 12/10/16.
 * Base ViewModel interface
 */

public interface ViewModel {
    void destroy();
}
