package com.rafaelbacelar.desafioandroid.viewmodel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.view.PullsActivity;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class RepositoryItemViewModel extends BaseObservable implements ViewModel {

    private Context context;
    Repository repository;

    public RepositoryItemViewModel(Context context, Repository repository) {
        this.context = context;
        this.repository = repository;
    }

    public String getName() {
        return repository.getName();
    }

    public String getDescription() {
        return repository.getDescription();
    }

    public String getStars() {
        return String.valueOf(repository.getStars());
    }

    public String getForks() {
        return String.valueOf(repository.getForks());
    }

    public String getUsername() {
        return repository.getOwner() != null ? repository.getOwner().getLogin() : "";
    }

    public String getUserAvatarUrl() {
        return repository.getOwner() != null ? repository.getOwner().getAvatarUrl() : "";
    }

    @BindingAdapter({"srcUrl"})
    public static void loadImageFromUrl(ImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .placeholder(R.drawable.person)
                .crossFade()
                .into(view);
    }

    public void onItemClick(View view) {
        context.startActivity(PullsActivity.newIntent(context, getUsername(), getName()));
    }

    // Allows recycling ViewModels within the recyclerview adapter
    public void setRepository(Repository repository) {
        this.repository = repository;
        notifyChange();
    }

    @Override
    public void destroy() {

    }
}
