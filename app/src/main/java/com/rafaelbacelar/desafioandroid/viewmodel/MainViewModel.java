package com.rafaelbacelar.desafioandroid.viewmodel;

import android.content.Context;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;

import com.rafaelbacelar.desafioandroid.DesafioApplication;
import com.rafaelbacelar.desafioandroid.model.GithubService;
import com.rafaelbacelar.desafioandroid.model.Repository;
import com.rafaelbacelar.desafioandroid.model.RepositoryList;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class MainViewModel implements ViewModel {
    private static final String TAG = "MainViewModel";

    public ObservableInt progressBarVisibility;

    private Context context;
    private Subscription subscription;
    private List<Repository> repositories;
    private DataListener dataListener;

    public MainViewModel(Context context, DataListener dataListener){
        this.context = context;
        this.dataListener = dataListener;
        this.repositories = new ArrayList<>();
        progressBarVisibility = new ObservableInt(View.INVISIBLE);
    }

    public void loadData(int page) {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        progressBarVisibility.set(View.VISIBLE);
        DesafioApplication application = DesafioApplication.get(context);
        GithubService githubService = application.getGithubService();
        subscription = githubService.getJavaRepositoriesByPage(page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.getScheduler())
                .subscribe(new Subscriber<RepositoryList>() {
                    @Override
                    public void onCompleted() {
                        if (dataListener != null)
                            dataListener.onRepositoriesChanged(repositories);

                        progressBarVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.e(TAG, "Error loading GitHub repositories ", error);
                        progressBarVisibility.set(View.INVISIBLE);

                        dataListener.onLoadingError(error);
                    }

                    @Override
                    public void onNext(RepositoryList repositories) {
                        if(repositories != null && repositories.getTotalCount() > 0)
                            MainViewModel.this.repositories.addAll(repositories.getItems());
                    }
                });
    }

    @Override
    public void destroy() {
        if(subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        subscription = null;
        context = null;
        dataListener = null;
    }

    public interface DataListener {
        void onRepositoriesChanged(List<Repository> repositories);
        void onLoadingError(Throwable error);
    }
}
