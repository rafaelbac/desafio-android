package com.rafaelbacelar.desafioandroid.viewmodel;

import android.content.Context;
import android.databinding.ObservableInt;
import android.util.Log;
import android.view.View;

import com.rafaelbacelar.desafioandroid.DesafioApplication;
import com.rafaelbacelar.desafioandroid.model.GithubService;
import com.rafaelbacelar.desafioandroid.model.Pull;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class PullsViewModel implements ViewModel {
    private static final String TAG = "PullsViewModel";

    public ObservableInt progressBarVisibility;

    private Context context;
    private Subscription subscription;
    private List<Pull> pulls;
    private DataListener dataListener;

    public PullsViewModel(Context context, DataListener dataListener){
        this.context = context;
        this.dataListener = dataListener;
        this.pulls = new ArrayList<>();
        progressBarVisibility = new ObservableInt(View.INVISIBLE);
    }

    public void loadData(String username, String repository) {
        if (subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        progressBarVisibility.set(View.VISIBLE);
        DesafioApplication application = DesafioApplication.get(context);
        GithubService githubService = application.getGithubService();
        subscription = githubService.getPullsByRepository(username, repository)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(application.getScheduler())
                .subscribe(new Subscriber<List<Pull>>() {
                    @Override
                    public void onCompleted() {
                        if (dataListener != null)
                            dataListener.onPullsChanged(pulls);

                        progressBarVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onError(Throwable error) {
                        Log.e(TAG, "Error loading GitHub pulls ", error);
                        progressBarVisibility.set(View.INVISIBLE);
                    }

                    @Override
                    public void onNext(List<Pull> pulls) {
                        if(pulls != null && pulls.size() > 0)
                            PullsViewModel.this.pulls = pulls;
                    }
                });
    }

    @Override
    public void destroy() {
        if(subscription != null && !subscription.isUnsubscribed())
            subscription.unsubscribe();

        subscription = null;
        context = null;
        dataListener = null;
    }

    public interface DataListener {
        void onPullsChanged(List<Pull> pulls);
    }
}
