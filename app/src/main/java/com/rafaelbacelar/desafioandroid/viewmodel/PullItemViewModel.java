package com.rafaelbacelar.desafioandroid.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.rafaelbacelar.desafioandroid.R;
import com.rafaelbacelar.desafioandroid.model.Pull;

import java.text.SimpleDateFormat;

/**
 * Created by rafaelbacelar on 12/10/16.
 */

public class PullItemViewModel extends BaseObservable implements ViewModel {

    private Context context;
    private Pull pull;
    private SimpleDateFormat dateFormat;

    public PullItemViewModel(Context context, Pull pull, SimpleDateFormat dateFormat) {
        this.context = context;
        this.pull = pull;
        this.dateFormat = dateFormat;
    }

    public String getTitle() {
        return pull.getTitle();
    }

    public String getBody() {
        return pull.getBody();
    }

    public String getCreatedAt() {
        return dateFormat.format(pull.getCreatedAt());
    }

    public String getUsername() {
        return pull.getUser() != null ? pull.getUser().getLogin() : "";
    }

    public String getUserAvatarUrl() {
        return pull.getUser() != null ? pull.getUser().getAvatarUrl() : "";
    }

    @BindingAdapter({"srcUrl"})
    public static void loadImageFromUrl(ImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .placeholder(R.drawable.person)
                .crossFade()
                .into(view);
    }

    public void onItemClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getUrl()));
        context.startActivity(browserIntent);
    }

    // Allows recycling ViewModels within the recyclerview adapter
    public void setPull(Pull pull) {
        this.pull = pull;
        notifyChange();
    }

    @Override
    public void destroy() {

    }
}
